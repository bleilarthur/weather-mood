# Weather Mood

This is the code challenge for A Big Circle.

## Before start

Run `npm install`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

